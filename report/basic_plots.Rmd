---
title: "basic_plots"
output: html_document
---

```{r}
library(titanicSurvival)
library(ggplot2)
library(forcats)
plot_data <- titanic_data %>% standardize_colnames() %>% unify_format()
```

```{r}
plot_data %>% 
  ggplot2::ggplot(aes(y = fct_infreq(deck))) +
  geom_bar() +
  ggplot2::ylab("Deck") + 
  ggplot2::xlab("Persons (N)")
```

```{r}
plot_data %>% 
  ggplot2::ggplot(aes(x = sex, y = fct_infreq(deck))) +
  geom_jitter(width = 0.1, height = 0) +
  ggplot2::ylab("Deck") + 
  ggplot2::xlab("Persons (N)")
```




## Sex distribution

```{r}
plot_data %>% 
  ggplot2::ggplot(aes(y = fct_infreq(sex))) +
  geom_bar() +
  ggplot2::ylab("Sex") + 
  ggplot2::xlab("Persons (N)")
```

```{r}
plot_data %>% 
  ggplot(aes(x = survived, y = sex)) +
  geom_jitter(width = 0.1, height = 0.1)
```


