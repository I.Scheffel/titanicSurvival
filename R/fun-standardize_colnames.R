#' creates desired colnames
#'
#' @param data_in raw titanic dataset
#'
#' @return dataset with desired columnnames
#' @export
standardize_colnames <- function(data_in) {

  old_names <- c("id" = "PassengerId",
                "class" = "Pclass")
  delete_names <- c("Name",
                   "Parch",
                   "Ticket")

  stopifnot(sample(delete_names) %in% colnames(data_in))

  data_out <- data_in %>%
    dplyr::rename(old_names) %>%
    dplyr::select(-delete_names) %>%
    dplyr::rename_all(tolower)
  return(data_out)

}
